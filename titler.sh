#!/bin/bash
echo
echo "titler: add a text title before your video. Dependencies: ImageMagick, ffmpeg, mencoder. License: GPLv3+."
echo "Usage: ./titler video.avi 'title of the video'"
echo
if [ $# -ne 2 ]; then exit; fi

width=`ffprobe -show_entries stream=width -of default=noprint_wrappers=1:nokey=1 "$1"`
height=`ffprobe -show_entries stream=height -of default=noprint_wrappers=1:nokey=1 "$1"`
fps=`ffprobe -show_entries stream=avg_frame_rate -of default=noprint_wrappers=1:nokey=1 "$1"|head -1`

convert -size "${width}x${height}" xc:"white" empty_slide.jpg
convert -size "${width}x" -pointsize 30 -gravity center caption:"$2" title_text.jpg
composite -gravity center -geometry +0+0 title_text.jpg empty_slide.jpg title_slide.jpg

ffmpeg -y -loop 1 -i title_slide.jpg -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -shortest -c:v libx264 -c:a libmp3lame -t 2 -r $fps title_video.avi

mencoder "$1" -ovc x264 -oac mp3lame -o video_converted.avi
mencoder -quiet -ovc x264 -oac mp3lame title_video.avi video_converted.avi -o "${1%.*}_with_title.mp4"
rm empty_slide.jpg title_text.jpg title_slide.jpg title_video.avi video_converted.avi
